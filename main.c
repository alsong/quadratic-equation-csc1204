#include <stdio.h>
#include <math.h>

void findRoots(double a, double b, double c) {
    if (a == 0) {
        printf("Invalid input. 'a' cannot be zero.\n");
        return;
    }

    double discriminant = b*b - 4*a*c;
    double sqrt_val = sqrt(fabs(discriminant));

    if (discriminant > 0) {
        printf("Roots are real and distinct.\n");
        printf("Root 1 = %.2lf\n", (-b + sqrt_val) / (2*a));
        printf("Root 2 = %.2lf\n", (-b - sqrt_val) / (2*a));
    } else if (discriminant == 0) {
        printf("Roots are real and equal.\n");
        printf("Root = %.2lf\n", -b / (2*a));
    } else { // discriminant < 0
        printf("Roots are complex.\n");
        printf("Root 1 = %.2lf + %.2lfi\n", -b / (2*a), sqrt_val / (2*a));
        printf("Root 2 = %.2lf - %.2lfi\n", -b / (2*a), sqrt_val / (2*a));
    }
}

int main() {
    double a, b, c;
    printf("Enter coefficients a, b and c: ");
    scanf("%lf %lf %lf", &a, &b, &c);
    findRoots(a, b, c);
    return 0;
}
